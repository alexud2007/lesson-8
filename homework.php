<?php

class TelegraphText {
    public $title;
    public $text;
    public $author;
    public $published;
    public $slug;

    public function __construct ($author, $slug)
    {
        $this->author = $author;
        $this->slug = $slug;
        $this->published = date("D - d - F - Y");
    }

    public function storeText ()
    {
        $textStorage = array(
            'title' => $this->title,
            'author' => $this->author,
            'text' => $this->text,
            'published' => $this->published
        );
        file_put_contents($this->slug, serialize($textStorage));

    }

    public function loadText ()
    {
        if(file_exists($this->slug));
        {
            $tempArray = unserialize(file_get_contents($this->slug));
            $this->title = $tempArray['title'];
            $this->author = $tempArray['author'];
            $this->text = $tempArray['text'];
            $this->published = $tempArray['published'];

            return $this->text;
        }
        return false;
    }

    public function editText ($title, $text)
    {
        $this->title = $title;
        $this->text = $text;
    }
}

$telegraphText = new TelegraphText('Kevin', 'telegraph-text.txt');
$telegraphText->editText('My First Telegraph Title', 'This is my first Telegraph text.');
$telegraphText->storeText();
echo $telegraphText->loadText();